bashbake
========

A vastly simpler and non-python dependant variant of commandline's flashbake.

When I was looking at using flashbake, I wanted to be able to track my word counts. There is a plugin for word counts with flashbake, but it is for scrivner files. I don't use scrivner and have no plan to.  The simple unix wc(1) command would do for my needs.

Like flashbake, bashbake uses git as the version control repository. Plugins are simple commands whose agregated output comprised the commit message.

Install
-------

Run the install.sh script to install bashbake in /usr/local/bin:

    $ sudo ./install.sh

Configuration
-------------

Bashbake is controlled by the .bashbake file in the root of your project. (The same directory that contains the .git directory).

Essentially, the .bashbake file is a shell script whose output creates the commit message.

Example - Wordcount for all text files:

    find . -type f -name *.txt | xargs wc -w

Running
-------

To run bashbake from cron, add the following to your crontab file:

    */15 * * * * bashbake ~/writing_project
